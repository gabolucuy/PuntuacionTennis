
public class TennisGame2 implements TennisGame
{
    public int P1point = 0;
    public int P2point = 0;
    
    public String P1res = "";
    public String P2res = "";
    private String player1Name;
    private String player2Name;

    public TennisGame2(String player1Name, String player2Name) {
        this.player1Name = player1Name;
        this.player2Name = player2Name;
    }

    public String getScore(){
        String score = "";
        int Player1Points = P1point;
		int Player2Points = P2point;
		score = player2Advantage(score, Player1Points, Player2Points);
        score = isGameDeuced(score, Player1Points, Player2Points);
      
        
        score = player1IsWinnig(score, Player1Points, Player2Points);
        score = player2IsWinnig(score, Player1Points, Player2Points); 
        score = playerWinningToCero(score, Player1Points, Player2Points);
        score = isGameInAdvantage(score, Player1Points, Player2Points);   
        score = playerWon(score, Player1Points, Player2Points);
        score = playerWon(score, Player1Points, Player2Points);
        return score;
    }

	private String player2IsWinnig(String score, int Player1Points, int Player2Points) {
		if (Player2Points>Player1Points && Player2Points < 4)
        {
			P1res=getLiteralScore(Player1Points);
	         P2res = getLiteralScore(Player2Points);
            score = P1res + "-" + P2res;
        }
		return score;
	}

	private String player1IsWinnig(String score, int Player1Points, int Player2Points) {
		if (Player1Points>Player2Points && Player1Points < 4)
        {
			P1res=getLiteralScore(Player1Points);
	        P2res = getLiteralScore(Player2Points);
            score = P1res + "-" + P2res;
        }
		return score;
	}

//	
	private String playerWinningToCero(String score, int Player1Points, int Player2Points){
		if (Player2Points > 0 && Player1Points==0 || Player1Points > 0 && Player2Points==0)
        { 
			
	         P1res=getLiteralScore(Player1Points);
	         P2res = getLiteralScore(Player2Points);
	         score = P1res + "-" + P2res;   
	    }
		
		return score;
	}

	private String getLiteralScore(int PlayerPoints){
		String score="";
		if (PlayerPoints==0)
            score = "Love";
		 if (PlayerPoints==1)
             score = "Fifteen";
         if (PlayerPoints==2)
             score = "Thirty";
         if (PlayerPoints==3)
             score = "Forty";
         return score;
	}
	private String isGameInAdvantage(String score, int Player1Points, int Player2Points){
		if (Player1Points > Player2Points && Player2Points >= 3)
        {
            score = "Advantage player1";
        }
		if (Player2Points > Player1Points && Player1Points >= 3)
        {
            score = "Advantage player2";
        }
		return score;
	}
	
	private String playerWon(String score, int Player1Points, int Player2Points) {
		if (Player1Points>=4 && Player2Points>=0 && (Player1Points-Player2Points)>=2)
        {
            score = "Win for player1";
        }
		if (Player2Points>=4 && Player1Points>=0 && (Player2Points-Player1Points)>=2)
        {
            score = "Win for player2";
        }
		return score;
	}

	private String isGameDeuced(String score, int Player1Points, int Player2Points) {
		if (Player1Points==Player2Points && Player1Points>=3)
            score = "Deuce";
		return score;
	}

	private String player2Advantage(String score, int Player1Points, int Player2Points) {
		if (Player1Points == Player2Points && Player1Points < 4)
        {
            if (Player1Points==0)
                score = "Love";
            if (Player1Points==1)
                score = "Fifteen";
            if (Player1Points==2)
                score = "Thirty";
            score += "-All";
        }
		return score;
	}
    
    public void SetP1Score(int number){
        
        for (int i = 0; i < number; i++)
        {
            P1Score();
        }
            
    }
    
    public void SetP2Score(int number){
        
        for (int i = 0; i < number; i++)
        {
            P2Score();
        }
            
    }
    
    public void P1Score(){
        P1point++;
    }
    
    public void P2Score(){
        P2point++;
    }

    public void wonPoint(String player) {
        if (player == "player1")
            P1Score();
        else
            P2Score();
    }
}